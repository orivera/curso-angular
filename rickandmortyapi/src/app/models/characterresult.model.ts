import {InfoModel} from "./info.model";
import {CharacterModel} from "./character.model";

export class CharacterResultModel{
  constructor(
    public info: InfoModel,
    public results: CharacterModel[]
  ) {
  }
}
