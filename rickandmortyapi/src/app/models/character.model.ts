import {OriginModel} from "./origin.model";
import {LocationModel} from "./location.model";

export class CharacterModel{
  constructor(
    public id: number,
    public name: string,
    public status: string,
    public species: string,
    public type: string,
    public gender: string,
    public origin: OriginModel,
    public location: LocationModel,
    public image: string,
    public episode: string[],
    public url: string,
    public created: string
  ) {

  }

}
