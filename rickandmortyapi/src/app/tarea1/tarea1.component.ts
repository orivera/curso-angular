import {Component, OnInit} from '@angular/core';
import {CharacterModel} from "../models/character.model";
import {CharacterService} from "../services/character.service";


@Component({
  selector: 'app-tarea1',
  templateUrl: './tarea1.component.html',
  styleUrls: ['./tarea1.component.css']
})
export class Tarea1Component implements OnInit {

  loading: boolean;
  personajes: CharacterModel[];

  constructor(public service: CharacterService) {
      console.log('Tarea1Component()');
    this.loading = true;
    service.obtener().subscribe(
      (data) => {
        this.personajes = data.results;
        console.log(data);
      },
      (err) => console.log(err),
      () => {
        console.log('listo!');
        this.loading = false;
      }
    );
  }

  ngOnInit() {
      console.log('Tarea1Component.ngOnInit()');
  }

}
