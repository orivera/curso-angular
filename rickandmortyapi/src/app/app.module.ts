import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {Tarea1Component} from './tarea1/tarea1.component';
import {CharacterService} from "./services/character.service";
import {HttpClientModule} from "@angular/common/http";
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import {MatCardModule} from "@angular/material/card";

@NgModule({
    declarations: [
        AppComponent,
        Tarea1Component
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        NoopAnimationsModule,
        MatCardModule
    ],
    providers: [CharacterService],
    bootstrap: [AppComponent]
})

export class AppModule {
}
