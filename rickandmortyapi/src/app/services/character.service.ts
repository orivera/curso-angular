import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from "rxjs";
import {CharacterResultModel} from "../models/characterresult.model";
import {CHARACTER_ENDPOINT} from "../config/config";

@Injectable({
    providedIn: 'root'
})
export class CharacterService {
  constructor(public http: HttpClient) {
  }

  obtener(): Observable<CharacterResultModel> {
    return this.http.get<CharacterResultModel>(CHARACTER_ENDPOINT);
  }
}
